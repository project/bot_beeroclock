<?php
/**
 * @file
 * Unit tests for the bot_beeroclock module
 */

/**
 * Bot BeerOclock unit test
 */
class BotBeeroclockUnitTest extends DrupalUnitTestCase {

  /**
   * Test info.
   */
  public static function getInfo() {
    return array(
      'name' => 'Bot BeerOClock tests ',
      'description' => 'Test the values returned by bot_beeroclock',
      'group' => 'PNX API',
    );
  }

  /**
   * Test setup.
   */
  public function setUp() {
    parent::setUp();
    // Enable modules.
    $this->enableModule('bot');
    $this->enableModule('bot_beeroclock');
  }

  /**
   * Tests for weekday beeroclock.
   */
  public function testWeekdayBeeroclock() {
    $datetime = new DateTime("2012-10-10 15:55:00");
    $output = bot_beeroclock_get_beeroclock($datetime);
    $expected = "1 hour and 5 minutes to go!";
    $this->assertEqual(
      $output,
      $expected,
      "Output differs. Expected: " . $expected . ", Got: " . $output
    );
  }

  /**
   * Tests for hours only beeroclock.
   */
  public function testHoursOnlyBeeroclock() {
    $datetime = new DateTime("2012-10-10 15:00:00");
    $output = bot_beeroclock_get_beeroclock($datetime);
    $expected = "2 hours to go!";
    $this->assertEqual(
      $output,
      $expected,
      "Output differs. Expected: " . $expected . ", Got: " . $output
    );
  }

  /**
   * Tests for minutes only beeroclock.
   */
  public function testMinutesOnlyBeeroclock() {
    $datetime = new DateTime("2012-10-10 16:05:00");
    $output = bot_beeroclock_get_beeroclock($datetime);
    $expected = "55 minutes to go!";
    $this->assertEqual(
      $output,
      $expected,
      "Output differs. Expected: " . $expected . ", Got: " . $output
    );
  }

  /**
   * Tests for seconds only beeroclock.
   */
  public function testSecondsOnlyBeeroclock() {
    $datetime = new DateTime("2012-10-10 16:59:25");
    $output = bot_beeroclock_get_beeroclock($datetime);
    $expected = "35 seconds to go!";
    $this->assertEqual(
      $output,
      $expected,
      "Output differs. Expected: " . $expected . ", Got: " . $output
    );
  }


  /**
   * Tests for weekday beeroclock.
   */
  public function testWeekdayNotBeeroclock() {
    $datetime = new DateTime("2012-10-10 18:00:00");
    $output = bot_beeroclock_get_beeroclock($datetime);
    $expected = "YES! It's beer o'clock!";
    $this->assertEqual(
      $output,
      $expected,
      "Output differs. Expected: " . $expected . ", Got: " . $output
    );
  }

  /**
   * Fake enables a module for the purpose of a unit test.
   *
   * @param string $name
   *   The module's machine name (i.e. ctools not Chaos Tools)
   */
  protected function enableModule($name) {
    $modules = module_list();
    $modules[$name] = $name;
    module_list(TRUE, FALSE, FALSE, $modules);
  }
}
